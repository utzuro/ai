class Activation_SoftMAX:
    def __init__(self):
        self.E = math.e #2.71828182846...
    def forward(self, inputs):
        self.outputs = []
        for input in inputs:
            self.outputs.append(self.E**input)

        norm_base = sum(self.outputs)
        norm_values = []
        for value in self.outputs:
            norm_values.append(value / norm_base)
